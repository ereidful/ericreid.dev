import Header from "../components/header"
import ProjectCard from "../components/projectCard"

export default function Home() {
  const smashBot = {
    title: `Super Smash Bros Discord Bot`,
    description: `A discord bot to generate tournament brackets based on inputted names`,
    gitLink: `https://gitlab.com/ereidful/smash-discord-bot`,
    siteLink: `#`
  }

  const reiderboard = {
    title: `Reiderboard`,
    description: `Live leaderboard for family fantasy golf game`,
    gitLink: `https://gitlab.com/ereidful/major-leaderboard`,
    siteLink: `https://www.reiderboard.com`
  }

  const excelToList = {
    title: `Excel to List`,
    description: `Utility for converting copied excel column to code-ready list`,
    gitLink: `https://gitlab.com/ereidful/excel-to-list`,
    siteLink: `excel-to-list`
  }

  const gortar = {
    title: `Guitar Resources`,
    description: `Collection of guitar tools and info`,
    gitLink: `https://gitlab.com/ereidful/geetar`,
    siteLink: `gortar`
  }

  const hopeReturned = {
    title: `Hope Returned`,
    description: `A charity website I created and maintained`,
    gitLink: `#`,
    siteLink: `www.hopereturned.org`
  }
  
  const timers = {
    title: `Timers`,
    description: `A workout util with multiple timers and set completion tracker`,
    gitLink: `https://gitlab.com/ereidful/timers`,
    siteLink: `#`
  }

  const codScraper = {
    title: `Call of Duty Stats`,
    description: `A scraper that compiles call of duty match history stats from cod.tracker.gg so that I could prove once and for all who the best player was`,
    gitLink: `https://gitlab.com/ereidful/cod-tracker-scraper`,
    siteLink: '#'
  }

  const bCorpSearch = {
    title: `B Corporation Search`,
    description: `A "search engine" with results coming exclusively from companies with a B Corporation certificate for more ethical shopping`,
    gitLink: '#',
    siteLink: '#'
  }

  return (
    <div>
      <Header />
      <div className="grid grid-cols-3 gap-10">
        <ProjectCard projectInfo={bCorpSearch} />
        <ProjectCard projectInfo={hopeReturned} />
        <ProjectCard projectInfo={reiderboard} />
        <ProjectCard projectInfo={smashBot} />
        <ProjectCard projectInfo={excelToList} />
        <ProjectCard projectInfo={gortar} />
        <ProjectCard projectInfo={timers} />
        <ProjectCard projectInfo={codScraper} />
      </div>
    </div>
  )
}
