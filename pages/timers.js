import { useState } from "react"

export default function Timers() {
    const [bigTime, setBigTime] = useState(0);
    const [bigStart, setBigStart] = useState(0);
    const [bigCurrent, setBigCurrent] = useState(0);
    const [bigDiffAtStop, setBigDiffAtStop] = useState(0);
    const [bigRunning, setBigRunning] = useState(false);
    const [bigInterval, setBigInterval] = useState(null);

    const [smallTime, setSmallTime] = useState(0);

    function handleBigStart() {
        setBigRunning(true);
        setBigStart(Date.now());
        setBigCurrent(Date.now() + bigDiffAtStop);
        setBigInterval(setInterval(() => {setBigCurrent(Date.now() + bigDiffAtStop)}, 100));
    }

    function handleBigStop() {
        setBigRunning(false);
        clearInterval(bigInterval);
        setBigDiffAtStop(bigCurrent - bigStart);
    }

    return (
        <div className="flex flex-col justify-between h-screen py-10 text-xl lg:text-6xl bg-purple-100">
            <div className="flex justify-center">
                {/* insert up arrow here */}
                <div className="flex flex-col justify-center items-center">
                    <p>{new Date((bigStart + (bigTime*1000)) - bigCurrent).toISOString().substr(11, 8)}</p>
                    <div className="flex text-xl gap-4">
                        <button onClick={() => handleBigStart()}>start</button>
                        <button onClick={() => handleBigStop()}>stop</button>
                        <button>reset</button>
                    </div>
                </div>
                {/* insert up arrow here */}
            </div>

            <div className="flex justify-center">
                {/* insert down arrow here */}
                <div className="flex flex-col justify-center items-center">
                    <div className="flex text-xl gap-4">
                        {/* plus icon that adds 5 seconds to timer */}
                        {/* minux icon that subtracts 5 seconds to timer */}
                    </div>
                    <p>00:02:15</p>
                    <div className="flex text-xl gap-4">
                        <button>start</button>
                        <button>stop</button>
                        <button>reset</button>
                    </div>
                </div>
                {/* insert down arrow here */}
            </div>

            <div className="flex justify-evenly py-10">
                <div className="set-no cursor-pointer px-8 py-6 border-2 border-black shadow-brutal rounded-md relative" id="set-1">1</div>
                <div className="set-no cursor-pointer px-8 py-6 border-2 border-black shadow-brutal rounded-md relative" id="set-2">2</div>
                <div className="set-no cursor-pointer px-8 py-6 border-2 border-black shadow-brutal rounded-md relative" id="set-3">3</div>
                <div className="set-no cursor-pointer px-8 py-6 border-2 border-black shadow-brutal rounded-md relative" id="set-4">4</div>
                <div className="set-no cursor-pointer px-8 py-6 border-2 border-black shadow-brutal rounded-md relative" id="set-5">5</div>
            </div>
        </div>
    )
}