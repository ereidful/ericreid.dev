

export default function ProjectCard(props) {
    return (
        <div className="shadow-brutal shadow-plum-100 border-2 border-neutral-700 p-5 flex flex-col justify-between">
            <h2>{props.projectInfo.title}</h2>
            <p>{props.projectInfo.description}</p>
            <div className="flex justify-start gap-6">
                <a href={props.projectInfo.gitLink} className="underline decoration-plum-100 decoration-2 hover:decoration-4">git</a>
                <a href={props.projectInfo.siteLink} className="underline decoration-plum-100 decoration-2 hover:decoration-4">go</a>
            </div>
        </div>
    )
}