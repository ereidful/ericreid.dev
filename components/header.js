

export default function Header() {
    return (
        <header className="flex justify-between text-4xl mb-20">
            <h1>Eric Reid</h1>
            <div>
                <a href="mailto:ereid08@gmail.com" className="underline decoration-plum-100 decoration-2 hover:decoration-4">contact</a>
            </div>
        </header>
    )
}