module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        beige: {
          100: '#F5F3EB'
        },
        coolgray: {
          100: '#919EA8'
        },
        lightblue: {
          100: '#DFEBF5'
        },
        plum: {
          100: '#9D91A8'
        },
        lavendar: {
          100: '#E9DCF5'
        }
      },
      boxShadow: {
        'brutal': '5px 5px 0',
      },
    },
  },
  plugins: [],
}
